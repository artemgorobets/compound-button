package com.artemgorobets.compoundbutton;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by artemgorobets on 7/24/14.
 */
public class CompoundButton extends LinearLayout {

    public Button button;

    private int mImageRegular;
    private int mImagePressed;
    private int mTextRegularColor;
    private int mTextPressedColor;
    private int viewTag = 1;

    private String mTitle;
    private ImageView imageView;
    private TextView textView;

    public CompoundButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);

        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.CompoundButton, 0, 0);

        try {
            mImageRegular       = typedArray.getResourceId(R.styleable.CompoundButton_imageRegular, 0);
            mImagePressed       = typedArray.getResourceId(R.styleable.CompoundButton_imagePressed, 0);

            mTextRegularColor   = typedArray.getInteger(R.styleable.CompoundButton_textRegularColor, Color.parseColor("#000000"));
            mTextPressedColor   = typedArray.getInteger(R.styleable.CompoundButton_textPressedColor, Color.parseColor("#ffffff"));

            mTitle              = typedArray.getString(R.styleable.CompoundButton_buttonTitle);

        } finally {
            typedArray.recycle();
        }

        createButton();
    }

    private void createButton() {

        RelativeLayout containerLayout = new RelativeLayout(getContext());
        LayoutParams containerParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        button = new Button(getContext());
        button.setBackgroundColor(getResources().getColor(R.color.ButtonRegularColor));
        button.setGravity(Gravity.CENTER);
        LayoutParams buttonParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, Gravity.CENTER);

        button.setOnTouchListener(listener);

        imageView = new ImageView(getContext());
        imageView.setId(viewTag);
        imageView.setImageDrawable(getResources().getDrawable(mImageRegular));

        RelativeLayout.LayoutParams imageParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        imageParams.addRule(RelativeLayout.CENTER_IN_PARENT);

        textView = new TextView(getContext());
        textView.setText(mTitle);
        RelativeLayout.LayoutParams textParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        textParams.addRule(RelativeLayout.BELOW, 1);
        textParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

        this.addView(containerLayout, containerParams);
        containerLayout.addView(button, buttonParams);
        containerLayout.addView(imageView, imageParams);
        containerLayout.addView(textView, textParams);

        containerLayout.requestLayout();

    }

    private void onButtonUp() {
        imageView.setImageDrawable(getResources().getDrawable(mImageRegular));
        textView.setTextColor(mTextRegularColor);
        button.setBackgroundColor(getResources().getColor(R.color.ButtonRegularColor));
        requestLayout();
    }

    private void onButtonPressed(){
        imageView.setImageDrawable(getResources().getDrawable(mImagePressed));
        textView.setTextColor(mTextPressedColor);
        button.setBackgroundColor(getResources().getColor(R.color.ButtonPressedColor));
        requestLayout();
    }

    OnTouchListener listener = new OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                onButtonPressed();
            }

            if (motionEvent.getAction() == MotionEvent.ACTION_UP){
                onButtonUp();
            }

            return false;
        }
    };


}
